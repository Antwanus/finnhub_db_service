package com.antoonvereecken.dbfinnhubservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DbFinnhubServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(DbFinnhubServiceApplication.class, args);
	}

}
